import lxml.etree
import lxml.html


def refine_name(name):
    return name[17: len(name) - 14]

def make_stuff_obj(name , price , img):
    stuff = {
        "err": 'OK',
        "name": name,
        "price": price,
        "img": img
    }
    return stuff

def find_stuff_with_xpath( htmlfile):


    htmltree = lxml.html.fromstring(htmlfile)
    stuff_image = htmltree.xpath((
                                 '//div[@id="main"]/div[@class="inner-wraper"]/div[@id="content"]/div[@id="product-page"]/section[@id="frmSecProductMain"]/div[@class="products-gallery"]/div[@id="frmPnlProductGallery"]/ul[@class="clearfix"]/li[4]/a[@class="productItem"]/img/@src'))
    stuff_name = htmltree.xpath((
                                '//section[@id="frmSecProductMain"]/div[@class="products-info"]/header[@class="clearfix"]/div[@class="info-header"]/h1/span/text()'))
    stuff_price = htmltree.xpath((
                                 '//div[@id="products-price-status"]/div[@id="frmPnlPayablePrice"]/span[@id="frmLblPayablePriceAmount"]/text()'))





    refined_name = refine_name(stuff_name[0])


    stuff_obj = make_stuff_obj(refined_name , stuff_price[0] , stuff_image[0])

    return stuff_obj
