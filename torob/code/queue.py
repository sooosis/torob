import pika



class queue:



    def __init__(self, name):
        self.name = name
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
        self.channel = self.connection.channel()
        self.channel.queue_declare(queue=self.name, durable=True)





    def feed(self , msg):
        self.channel.basic_publish(exchange='',
                              routing_key=self.name,
                              body=msg,
                              properties=pika.BasicProperties(
                                  delivery_mode=2,  # make message persistent
                              ))




    def consume(self , funcname):
        self.channel.basic_qos(prefetch_count=1)
        self.channel.basic_consume(funcname,
                              queue=self.name)

        self.channel.start_consuming()






