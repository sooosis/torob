import requests , json
import sys
from queue import queue
from xpath import find_stuff_with_xpath







#create stuff for making its json obj
def find_stuff(response , err):

    if response and response.status_code ==  200 :
        data = response.json()
        htmlt = data['html']
        found_stuff = find_stuff_with_xpath(htmlt)
    else :
        found_stuff = {
            "err" : err ,
            "name": [],
            "price": [],
            "img": []
            }

    return found_stuff


#sending request to splash and gettin its response
def get_splash_response(url):
    response = requests.get(
        downloader_url + '/render.json?url=' + url + '&wait=6&images=0&timeout=45&html=1',
        auth=('user', 'userpass'))
    return response



#queue consumer act
def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)
    url = body
    error = []
    splash_response = ''

    try :
            splash_response = get_splash_response(url)
    except:
            error = sys.exc_info()[0]

    if error == [] :
        error = splash_response.status_code

    stuff = find_stuff(splash_response , error)

    stuff_json = json.dumps(stuff)
    stuff_objects_queue.feed(stuff_json)

    print ("DONE!")



    ch.basic_ack(delivery_tag = method.delivery_tag)







downloader_url = 'http://104.251.212.43:8050'
print(' [*] Waiting for messages. To exit press CTRL+C')
stuff_objects_queue = queue('stuff_queue')
queue_of_urls = queue('task')
queue_of_urls.consume(callback)









